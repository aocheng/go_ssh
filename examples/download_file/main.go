package main

import (
	"fmt"
	"gitee.com/aocheng/go_ssh"
)

func main() {
	s := zdpgo_ssh.NewWithConfig(&zdpgo_ssh.Config{
		Host:     "192.168.33.10",
		Port:     22,
		Username: "admin",
		Password: "admin",
	})
	output, err := s.Sudo("ls -lah")
	fmt.Printf("%v\n%v", output, err)

	// 下载文件
	s.DownloadFile("README111.md", "README111.md")

	output, err = s.Sudo("ls -lah")
	fmt.Printf("%v\n%v", output, err)
}
