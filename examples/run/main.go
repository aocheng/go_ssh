package main

import (
	"fmt"
	"gitee.com/aocheng/go_ssh"
)

func main() {
	// 创建对象
	s := zdpgo_ssh.NewWithConfig(&zdpgo_ssh.Config{
		Host:     "192.168.33.10",
		Port:     22,
		Username: "admin",
		Password: "admin",
	})

	// 进行连接
	output, err := s.Run("free -h")

	// 查看命令结果
	fmt.Printf("%v\n%v", output, err)

	// 查看健康状态
	fmt.Println(s.Status())
}
